﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Input;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641
using Windows.UI.Xaml.Shapes;
using Microsoft.VisualBasic;

namespace ThePriceIsRight
{
    public sealed partial class MainPage : Page
    {
        private const int NUMBER_OF_SECTIONS_ON_WHEEL = 20;
        const int WHEEL_SECTION_HEIGHT = 100;

        private bool _shouldBlockSpinning = false;
        Canvas _spinBlockerRectangle = null;
        
        List<Canvas> _canvasList = new List<Canvas>();

        public MainPage()
        {
            this.InitializeComponent();

            Loaded += Page_Loaded;
            Unloaded += Page_Unloaded;

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var sb = StatusBar.GetForCurrentView();
            sb.BackgroundColor = Colors.Black;
            sb.BackgroundOpacity = 1.0;

            var backgroundColorList = new List<Color>()
            {
                Colors.Black,
                Colors.Black,
                Colors.LightGreen,
                Colors.Black,
                Colors.LightGreen,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
                Colors.Black,
            };

            var textColorList = new List<Color>()
            {
                Colors.White,
                Colors.White,
                Colors.Green,
                Colors.Red,
                Colors.Green,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
            };

            var valueList = new List<string>()
            {
                "35",
                "80",
                "15",
                "100",
                "5",
                "90",
                "25",
                "70",
                "45",
                "10",
                "65",
                "30",
                "85",
                "50",
                "95",
                "55",
                "75",
                "40",
                "20",
                "60"
            };

            for (int i = 0; i < NUMBER_OF_SECTIONS_ON_WHEEL; i++)
            {
                Canvas canvas = new Canvas();
                canvas.ManipulationMode = ManipulationModes.TranslateY | ManipulationModes.TranslateInertia ;
                canvas.IsHitTestVisible = true;

                canvas.ManipulationStarted += OnManipulationStarted;
                canvas.ManipulationDelta += OnManipulationDelta;
                canvas.ManipulationCompleted += OnManipulationCompleted;

                Canvas.SetTop(canvas, WHEEL_SECTION_HEIGHT * i);
                Canvas.SetLeft(canvas, 0);
                canvas.Width = 400;
                canvas.Height = WHEEL_SECTION_HEIGHT;

                canvas.Background = new SolidColorBrush(backgroundColorList[i]);
                canvas.Visibility = Visibility.Visible;

                string onesDigitString = null;
                string tensDigitString = null;
                string hundredsDigitString = null;

                if (valueList[i] == "100")
                {
                    onesDigitString = "red0";
                    tensDigitString = "red0";
                    hundredsDigitString = "red1";
                } 
                else if (valueList[i] == "5")
                {
                    onesDigitString = "teal5";
                }
                else if (valueList[i] == "15")
                {
                    onesDigitString = "teal5";
                    tensDigitString = "teal1";
                }
                else
                {
                    onesDigitString = ReverseString(valueList[i]).Substring(0, 1);
                    if (valueList[i].Length > 1)
                    {
                        tensDigitString = ReverseString(valueList[i]).Substring(1, 1);
                    }
                }

                Image onesDigitImage = new Image();
                var onesDigitImageSource = new BitmapImage(new Uri("ms-appx:///Assets/" + onesDigitString + ".png", UriKind.RelativeOrAbsolute));
                onesDigitImage.Source = onesDigitImageSource;

                Image tensDigitImage = null;
                if (tensDigitString != null)
                {
                    tensDigitImage = new Image();
                    var tensDigitImageSource =
                        new BitmapImage(new Uri("ms-appx:///Assets/" + tensDigitString + ".png",
                            UriKind.RelativeOrAbsolute));
                    tensDigitImage.Source = tensDigitImageSource;
                }

                Image hundredsDigitImage = null;
                if (hundredsDigitString != null)
                {
                    hundredsDigitImage = new Image();
                    var hundredsDigitImageSource =
                        new BitmapImage(new Uri("ms-appx:///Assets/" + hundredsDigitString + ".png",
                            UriKind.RelativeOrAbsolute));
                    hundredsDigitImage.Source = hundredsDigitImageSource;
                }

                TextBlock textBlock = new TextBlock();
                textBlock.TextAlignment = TextAlignment.Center;
                textBlock.FontSize = 35;
                textBlock.Width = canvas.ActualWidth;
                textBlock.Text = valueList[i];
                textBlock.Foreground = new SolidColorBrush(textColorList[i]);
                Canvas.SetTop(textBlock, 35);
                
                Rectangle topTrim = new Rectangle();
                topTrim.Fill = new SolidColorBrush(Colors.Gray);
                topTrim.Width = RootCanvas.ActualWidth;
                topTrim.Height = 3;
                Canvas.SetTop(topTrim, 0);
                Rectangle bottomTrim = new Rectangle();
                bottomTrim.Fill = new SolidColorBrush(Colors.Gray);
                bottomTrim.Width = RootCanvas.ActualWidth;
                bottomTrim.Height = 3;
                Canvas.SetTop(bottomTrim, WHEEL_SECTION_HEIGHT-3);
                
                canvas.Children.Add(topTrim);
                canvas.Children.Add(textBlock);
                canvas.Children.Add(bottomTrim);
                alignAndAddNumberImagesInCanvas(canvas, hundredsDigitImage, tensDigitImage, onesDigitImage);

                RootCanvas.Children.Add(canvas);
                _canvasList.Add(canvas);
            }
        }

        void alignAndAddNumberImagesInCanvas(Canvas canvas, Image hundredsDigitImage, Image tensDigitImage, Image onesDigitImage)
        {
            var canvasWidth = canvas.ActualWidth;

            if (hundredsDigitImage == null && tensDigitImage == null)
            {
                //just center the ones digit
                Canvas.SetTop(onesDigitImage, 40);
                Canvas.SetLeft(onesDigitImage, (canvasWidth/2.0f) - 12);

                canvas.Children.Add(onesDigitImage);
            }
            else if (hundredsDigitImage == null)
            {
                //center tens and ones
                Canvas.SetTop(tensDigitImage, 40);
                Canvas.SetTop(onesDigitImage, 40);
                Canvas.SetLeft(tensDigitImage, (canvasWidth / 2.0f) - 23);
                Canvas.SetLeft(onesDigitImage, (canvasWidth / 2.0f));

                canvas.Children.Add(tensDigitImage);
                canvas.Children.Add(onesDigitImage);
            }
            else
            {
                //center all three
                Canvas.SetTop(hundredsDigitImage, 40);
                Canvas.SetTop(tensDigitImage, 40);
                Canvas.SetTop(onesDigitImage, 40);
                Canvas.SetLeft(hundredsDigitImage, (canvasWidth / 2.0f) - 35);
                Canvas.SetLeft(tensDigitImage, (canvasWidth / 2.0f) - 12);
                Canvas.SetLeft(onesDigitImage, (canvasWidth / 2.0f) + 12);

                canvas.Children.Add(hundredsDigitImage);
                canvas.Children.Add(tensDigitImage);
                canvas.Children.Add(onesDigitImage);
            }
        }

        void setRectanglesTouchable(bool shouldBeTouchable)
        {
            foreach (var rectangle in _canvasList)
            {
                rectangle.IsHitTestVisible = shouldBeTouchable;
            }
        }

        void blockSpinning()
        {
            if (_shouldBlockSpinning == false)
            {
                _spinBlockerRectangle = new Canvas();
                Canvas.SetTop(_spinBlockerRectangle, 0);
                Canvas.SetLeft(_spinBlockerRectangle, 0);
                _spinBlockerRectangle.IsHitTestVisible = true;
                _spinBlockerRectangle.Background = new SolidColorBrush(Color.FromArgb(1, 1, 1, 1));
                _spinBlockerRectangle.Visibility = Visibility.Visible;
                _spinBlockerRectangle.Width = RootCanvas.ActualWidth;
                _spinBlockerRectangle.Height = RootCanvas.ActualHeight;
                RootCanvas.Children.Add(_spinBlockerRectangle);

                _shouldBlockSpinning = true;
                Debug.WriteLine("block spinning");
            }
        }

        void unblockSpinning()
        {
            if (_shouldBlockSpinning == true)
            {
                RootCanvas.Children.Remove(_spinBlockerRectangle);
                _shouldBlockSpinning = false;
                Debug.WriteLine("open spinning again");
            }
        }

        private void OnManipulationStarted(object sender, ManipulationStartedRoutedEventArgs manipulationStartedRoutedEventArgs)
        {
           
        }

        private void OnManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            if (e.Delta.Translation.Y < 0)
            {
                return;
            }

            if (e.Delta.Translation.Y > 100)
            {
                blockSpinning();
            }

            foreach (Canvas canvas in _canvasList)
            {
                double currentTop = Canvas.GetTop(canvas);
                double finalTop = currentTop + e.Delta.Translation.Y;
                Canvas.SetTop(canvas, finalTop);

                if (finalTop > (WHEEL_SECTION_HEIGHT*(NUMBER_OF_SECTIONS_ON_WHEEL-1)))
                {
                    double overShotDistance = finalTop - (WHEEL_SECTION_HEIGHT * (NUMBER_OF_SECTIONS_ON_WHEEL-1));
                    Canvas.SetTop(canvas, overShotDistance - WHEEL_SECTION_HEIGHT);
                }
            }
        }

        private void OnManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            unblockSpinning();
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
          
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

        }


        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
        
    }
}
